@extends('layouts.app')

@section('content')

  @if (isset($errors) && count($errors) > 0)
    <div class="alert alert-danger">
      @foreach ($errors->all() as $error)
        <p>{{ $error }}</p>
      @endforeach
    </div>
  @endif

<pagina tamanho="12">
    <painel titulo="{{ $title }}">
        @if (isset($authors))
            {!! Form::model($authors, ['route' => ['author.update', $authors->id], 'class' => 'Form', 'method' => 'PUT']) !!}
    {{--        {!! Form::hidden('updated_by',Auth::user()->id) !!}--}}
        @else
            {!! Form::open(['route' => 'author.store', 'class' => 'form']) !!}
    {{--        {!! Form::hidden('created_by',Auth::user()->id) !!}--}}
        @endif

        <div class="form-group">
            {!! Form::label('nm_autor', 'Igreja:'); !!}
            {!! Form::text('nm_autor', null, ['class' => 'form-control', 'placeholder' => 'Autor']) !!}<br>
            {!! Form::label('status_id', 'Status:'); !!}
            @if(isset($authors))
                {!! Form::select('status_id',$authors->status->pluck('descricao','id'), null, ['class' => 'form-control']) !!}<br>
            @else
                {!! Form::select('status_id',$statuses->pluck('descricao','id'), null, ['class' => 'form-control']) !!}<br>
            @endif

            {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
        {{--    {{ Form::button('<i class="glyphicon glyphicon-floppy-disk"> Salvar</i>', ['type' => 'submit', 'class' => 'btn btn-primary'] )  }}--}}
            {!! Form::close() !!}
        </div>
    </painel>
</pagina>
@endsection
