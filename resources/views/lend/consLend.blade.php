@extends('layouts.app')

@section('content')

<pagina tamanho="12">
    <painel titulo="{{ $title }}">

    @if (isset($errors) && count($errors) > 0)
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
        </div>
    @endif

    <a href="{{ route('emprestimo.create')}}" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Cadastrar</a>
    <br><br>
    <table class="table table-striped">
        <tr>
            <th width="50px">@sortablelink('id','#')</th>
            <th>@sortablelink('titulo_id','Livro')</th>
            <th>@sortablelink('membro_id','Emprestado a:')</th>
            <th>@sortablelink('dataemprestimo','Data de Empréstimo:')</th>
            <th>@sortablelink('tempoemprestimo','Previsão de Devolução:')</th>
            @if(Request::segment(1) == 'devolvidos')
                <th>@sortablelink('datadevolvido','Devolvido em:')</th>
            @elseif(Request::segment(1) == 'emprestimo')
                <th width="150px">Ações</th>
            @endif
        </tr>
        @foreach ($lends as $lend)
            <tr>
                <td>{{ $lend->id }}</td>
                <td>{{ $lend->book->titulo }}</td>
                <td>{{ $lend->member->nome }}</td>
                <td>{{ $lend->dataemprestimo_formatted }}</td>
                <td>
                        @if ($lend->dataemprestimo->addDays($lend->tempoemprestimo) < $now)
                            <span class="label label-danger">{{ $lend->dataemprestimo->addDays($lend->tempoemprestimo)->format('d/m/Y') }}</span>
                        @else
                            {{ $lend->dataemprestimo->addDays($lend->tempoemprestimo)->format('d/m/Y') }}
                        @endif
                </td>
                @if(Request::segment(1) == 'devolvidos')
                    <td>{{ $lend->datadevolvido_formatted }}</td>
                @elseif(Request::segment(1) == 'emprestimo')
                <td>
                    <button type="button" title="DEVOLVER" class="btn btn-sm btn-default" data-toggle="modal" data-target="#devolver{{ $lend->id }}">
                        <span class="glyphicon glyphicon-check"></span>
                    </button>
                    <a class = "btn btn-sm btn-default" href="{{ route('emprestimo.edit',$lend->id)}}">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>
                    <button type="button" title="EXCLUIR" class="btn btn-sm btn-default" data-toggle="modal" data-target="#excluir{{ $lend->id }}">
                        <span class="glyphicon glyphicon-trash"></span>
                    </button>
                @endif
                    <!-- Modal EXCLUIR-->
                    <div class="modal fade" id="excluir{{$lend->id}}" tabindex="-1" role="dialog" aria-labelledby="excluir">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">

                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Deseja excluir?</h4>
                                </div>
                                <div class="modal-body">
                                    <div align="center">
                                        <p>Excluir empréstimo do livro <b>{{ $lend->book->titulo }}</b> a <b>{{ $lend->member->nome }}</b></p>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    {!! Form::open(['route'=> ['emprestimo.destroy',$lend->id], 'method'=>'DELETE']) !!}
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                    <button type="submit" class = "btn btn-danger"> <span class="glyphicon glyphicon-trash"></span> Excluir </button>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal EXCLUIR-->
                    <!-- Modal DEVOLVER-->
                    <div class="modal fade" id="devolver{{$lend->id}}" tabindex="-1" role="dialog" aria-labelledby="devolver">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">

                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Devolver o livro</h4>
                                </div>
                                <div class="modal-body">
                                    <div align="center">
                                        <p>Devolver o livro <b>{{ $lend->book->titulo }}</b> emprestado a <b>{{ $lend->member->nome }}</b></p>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    {!! Form::open(['route' => ['emprestimo.update', $lend->id], 'class' => 'Form', 'method' => 'PUT']) !!}
                                    {!! Form::hidden('datadevolvido', $lend->datadevolver()) !!}<br>

                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                    <button type="submit" class = "btn btn-success"> <span class="glyphicon glyphicon-check"></span> Devolver </button>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal DEVOLVER-->
                    </div>
                </td>
            </tr>

        @endforeach
    </table>
    {{-- {!! $lends->appends(\Request::except('page'))->render() !!} --}}
        <hr>
        <div align="right">
            @if(Request::segment(1) == 'emprestimo')
                <a href="{{ route('devolvidos')}}" class="btn btn-success"><span class="glyphicon glyphicon-check"></span> Devolvidos</a>
            @else
                <a href="{{ route('emprestimo.index')}}" class="btn btn-info"><span class="glyphicon glyphicon-chevron-left"></span> Emprestados</a>
            @endif
        </div>
    </painel>

</pagina>
@endsection
