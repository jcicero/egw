@extends('layouts.app')

@section('content')

<pagina tamanho="12">
    <painel titulo="{{ $title }}">

      @if (Session::has('success'))
        <div class="alert alert-success">
          <ul>
            <li>{!! Session::get('success') !!}</li>
          </ul>
        </div>
      @endif
        @if (isset($errors) && count($errors) > 0)
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        @endif

    @if (isset($lends))
        {!! Form::model($lends, ['route' => ['emprestimo.update', $lends->id], 'class' => 'Form', 'method' => 'PUT']) !!}
{{--        {!! Form::hidden('updated_by',Auth::user()->id) !!}--}}
    @else
        {!! Form::open(['route' => 'emprestimo.store', 'class' => 'form']) !!}
{{--        {!! Form::hidden('created_by',Auth::user()->id) !!}--}}
    @endif

    <div class="form-group">
        {!! Form::label('member_id', 'Membro:'); !!}
        {!! Form::select('member_id', $members,null, ['class' => 'form-control js-member-placeholder']) !!}
        {!! Form::label('book_id', 'Livro:'); !!}
        {!! Form::select('book_id', $books->pluck('titulo_formatted','id'),null, ['class' => 'form-control js-book-placeholder']) !!}
        {!! Form::label('dataemprestimo', 'Data de Empréstimo:'); !!}
        <div class="form-group form-inline">
            @if (isset($lends))
                <input class="form-control" placeholder="Data de Empréstimo" name="dataemprestimo" type="date" value="{{ $lends->dataemprestimo->format('Y-m-d') }}" id="dataemprestimo">
            @else
                {!! Form::date('dataemprestimo',null, ['class' => 'form-control']) !!}
            @endif

          {!! Form::label('tempoemprestimo', 'Tempo de Empréstimo (em dias):'); !!}
          {!! Form::number('tempoemprestimo',30, ['class' => 'form-control','placeholder'=>'Tempo de leituta em dias']) !!}
        </div>
        {!! Form::label('obs', 'Observação:'); !!}
        {!! Form::text('obs',null, ['class' => 'form-control','placeholder'=>'Observação']) !!}<br>
        {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
    {{--    {{ Form::button('<i class="glyphicon glyphicon-floppy-disk"> Salvar</i>', ['type' => 'submit', 'class' => 'btn btn-primary'] )  }}--}}
        {!! Form::close() !!}
    {{--@endshield--}}
    </div>

  </painel>
</pagina>
@endsection

@section('js')

    <script>
        $(".js-member-placeholder").select2({
            placeholder: "Selecione o membro...",
            allowClear: true
        });
        $(".js-book-placeholder").select2({
            placeholder: "Selecione o livro...",
            allowClear: true
        });

    </script>

@endsection
