@extends('layouts.app')

@section('content')

<pagina tamanho="10">
    <painel titulo="Dashboard">

        <div class="row">
            <div class="col-md-3">
                <caixa qtd="{{ $books->count('id') }}" titulo="Livros" url="{{ route('book.index') }}" cor="green" icone="ion ion-ios-bookmarks"></caixa>
            </div>
            <div class="col-md-3">
                <caixa qtd="{{ $lends->count('id') }}" titulo="Emprestados" url="{{ route('emprestimo.index') }}" cor="red" icone="ion ion-briefcase"></caixa>
            </div>
            <div class="col-md-3">
                <caixa qtd="{{ $members->count('id') }}" titulo="Membros" url="{{ route('member.index') }}" cor="blue" icone="ion ion-person-stalker"></caixa>
            </div>
            <div class="col-md-3">
                <caixa qtd="{{ $authors->count('id') }}" titulo="Autores" url="{{ route('author.index') }}" cor="orange" icone="ion ion-person"></caixa>
            </div>
        </div>
    </painel>
</pagina>

@endsection
