@extends('layouts.app')

@section('content')


  <pagina tamanho="12">
      <painel titulo="{{ $title }}">

    @if (isset($books))
        {!! Form::model($books, ['route' => ['book.update', $books->id], 'class' => 'Form', 'method' => 'PUT']) !!}
{{--        {!! Form::hidden('updated_by',Auth::user()->id) !!}--}}
    @else
        {!! Form::open(['route' => 'book.store', 'class' => 'form']) !!}
{{--        {!! Form::hidden('created_by',Auth::user()->id) !!}--}}
    @endif
            <div class="form-group">
                {!! Form::label('título', 'Título:'); !!}
                {!! Form::textarea('titulo', null, ['class' => 'form-control', 'placeholder' => 'Título completo','size' => '30x2']); !!}
            </div>
            <div class="form-group">
                {!! Form::label('author_id','Autor:') !!}
        {{--        {!! Form::select('author_id',$authors->pluck('nm_autor','id'),null,['class'=>'js-autor-placeholder js-autor form-control']) !!}--}}
                <select id="author_id" name="author_id" class="js-autor-placeholder form-control" aria-hidden="true">
                    <option></option>
                    @foreach($authors as $author)
                        <option value="{{ $author->id }}">{{ $author->nm_autor }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                {!! Form::label('publisher_id','Editora:'); !!}
        {{--        {!! Form::select('publisher_id',$publishers->pluck('editora','id'),null,['class'=>'js-editora form-control','placeholder'=>'Selecione uma editora...']); !!}--}}
                <select id="publisher_id" name="publisher_id" class="js-editora-placeholder form-control" aria-hidden="true">
                    <option></option>
                    @foreach($publishers as $publisher)
                        <option value="{{ $publisher->id }}">{{ $publisher->editora }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                {!! Form::label('datapublicacao', 'Ano de Publicação:'); !!}
                {!! Form::number('datapublicacao', null, ['class' => 'form-control','placeholder'=>'Ano de publicação']); !!}
            </div>
            <div class="form-group">
                {!! Form::label('edicao','Edição:'); !!}
                {!! Form::text('edicao',null, ['class' => 'form-control','placeholder'=>'Número da edição']); !!}
            </div>
            <div class="form-group">
                {!! Form::label('paginas','Número de páginas:'); !!}
                {!! Form::number('paginas',null,['class'=>'form-control','placeholder'=>'Número de páginas']); !!}
            </div>
            <div class="form-group">
                {!! Form::label('resumo','Resumo:') !!}
                {!! Form::textarea('resumo',null,['class'=>'form-control','placeholder'=>'Resumo do livro']); !!}
            </div>
            <div class="form-group">
                {!! Form::label('dataaquisicao', 'Data de Aquisição:'); !!}
                {!! Form::date('dataaquisicao', null, ['class' => 'form-control']); !!}
            </div>
            <div class="form-group">
                {!! Form::label('emprestimo','Disponível para empréstimo?') !!}
                {!! Form::radio('emprestimo', '1',true);!!} Sim
                {!! Form::radio('emprestimo', '0');!!} Não
            </div>
            <div class="form-group">
                {!! Form::label('preco','Preço de compra:') !!}
                {!! Form::text('preco',null,['class'=>'form-control','placeholder'=>'Valor de compra em R$']) !!}
                {!! Form::hidden('status_id','1') !!}
            </div>
                <br>
                {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
            {{--    {{ Form::button('<i class="glyphicon glyphicon-floppy-disk"> Salvar</i>', ['type' => 'submit', 'class' => 'btn btn-primary'] )  }}--}}
                {!! Form::close() !!}
            </div>
    </painel>
</pagina>
@endsection

@section('js')

    <script>
        $(".js-autor-placeholder").select2({
            placeholder: "Selecione o autor...",
            allowClear: true
        });
        $(".js-editora-placeholder").select2({
            placeholder: "Selecione a editora...",
            allowClear: true
        });

    </script>

@endsection
