@extends('layouts.app')

@section('content')

    @if (isset($errors) && count($errors) > 0)
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
        </div>
    @endif
<pagina tamanho="12">
    <painel titulo="{{ $title }}">

        <a href="{{ route('book.create')}}" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Cadastrar</a>
        <br><br>
        <table class="table table-striped">
            <tr>
                <th width="50px">@sortablelink('id','#')</th>
                <th>@sortablelink('titulo','Título')</th>
                <th>@sortablelink('paginas','Páginas')</th>
                <th>@sortablelink('author_id','Autor')</th>
                <th>@sortablelink('datapublicacao','Publicado em')</th>
                <th>@sortablelink('emprestimo','Disponível para empréstimo?')</th>
                <th width="100px">Ações</th>
            </tr>
            @foreach ($books as $book)
                <tr>
                    <td>{{ $book->id }}</td>
                    <td>{{ $book->titulo }}</td>
                    <td>{{ $book->paginas }}</td>
                    <td>{{ $book->author->nm_autor }}</td>
                    <td>{{ $book->datapublicacao }}</td>
                    <td>{{ $book->emprestimo }}</td>
                    <td>
                        {{--@shield('book.editar')--}}
                        <a class = "btn btn-sm btn-default" href="{{ route('book.edit',$book->id)}}">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                        {{--@endshield--}}
                        <button type="button" title="EXCLUIR" class="btn btn-sm btn-default" data-toggle="modal" data-target="#excluir{{ $book->id }}">
                            <span class="glyphicon glyphicon-trash"></span>
                        </button>

                        <!-- Modal EXCLUIR-->
                        <div class="modal fade" id="excluir{{$book->id}}" tabindex="-1" role="dialog" aria-labelledby="excluir">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">

                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Deseja excluir?</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div align="center">
                                            <b>{{ $book->igreja }}</b>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        {!! Form::open(['route'=> ['book.destroy',$book->id], 'method'=>'DELETE']) !!}
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                        <button type="submit" class = "btn btn-danger"> <span class="glyphicon glyphicon-trash"></span> Excluir </button>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </td>
                </tr>

            @endforeach
        </table>
        {!! $books->appends(\Request::except('page'))->render() !!}
      </painel>
  </pagina>
@endsection
