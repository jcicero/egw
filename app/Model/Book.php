<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Book extends Model
{
    use SoftDeletes, Sortable;

    protected $dates = ['deleted_at','dataaquisicao'];

    protected $sortable = ['id','titulo','paginas','datapublicacao','emprestimo'];

    protected $fillable = ['titulo','datapublicacao','dataaquisicao','doador',
                            'edicao','emprestimo','paginas','resumo','status_id',
                            'author_id','publisher_id','preco'];

    public function getEmprestimoAttribute($value)
    {
        if($value == 1)
            {
                return 'Sim';
            }
        else
            {
                return 'Não';
            }
    }

    // public function getDatapublicacaoAttribute($value)
    // {
    //     return Carbon::parse($value)->format('Y');
    // }

    public function author()
    {
        return $this->belongsTo('App\Model\Author');
    }

    public function getTituloFormattedAttribute()
    {
        $titulo = $this->attributes['titulo'];
        $edicao = $this->attributes['edicao'];

        return $titulo . ' - ' . $edicao . 'ª edição';
    }
}
