<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Lend extends Model
{
  use SoftDeletes;

  protected $dates = ['deleted_at','dataemprestimo'];

  protected $fillable = ['book_id','member_id','dataemprestimo','tempoemprestimo','obs','datadevolvido'];

  public function book()
  {
    return $this->belongsTo('App\Model\Book');
  }

  public function member()
  {
    return $this->belongsTo('App\Model\Member');
  }

  public function getDataemprestimoFormattedAttribute()
  {
      $dt = $this->attributes['dataemprestimo'];
      return Carbon::parse($dt)->format('d/m/Y');
  }

  public function getDatadevolvidoFormattedAttribute()
  {
      $dt = $this->attributes['datadevolvido'];
      return Carbon::parse($dt)->format('d/m/Y');
  }

  public function now()
  {
      date_default_timezone_set('America/Sao_Paulo');
      $date = date('d/m/Y');
      return $date;
  }

  public function datadevolver()
  {
      date_default_timezone_set('America/Sao_Paulo');
      $date = date('Y-m-d');
      return $date;
  }
}
