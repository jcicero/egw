<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Lend;
use App\Model\Member;
use App\Model\Book;
use Carbon\Carbon;

class LendController extends Controller
{
  private $lend;

  public function __construct(Lend $lend)
  {
      $this->lend = $lend;
  }

  public function index()
  {
      $lends    = Lend::all()->sortBy('dataemprestimo')->where('datadevolvido','=','');
      $now      = Carbon::now();
      $title    = 'Empréstimos';

      return view('lend.consLend', compact('title', 'lends','now'));
  }

    public function devolvidos()
    {
        $lends    = Lend::all()->sortBy('dataemprestimo')->where('datadevolvido','!=','');
        $now      = Carbon::now();
        $title    = 'Empréstimos - Devolvidos';

        return view('lend.consLend', compact('title', 'lends','now'));
    }

  public function create()
  {
      $members  =  Member::pluck('nome','id')->sortBy('nome');
      $books    =  Book::all()->sortBy('titulo_formatted');
      $title    = 'Empréstimos';

      return view('lend.cadLend', compact('title', 'members','books'));
  }

  public function store(Request $request)
  {
      $dataForm = $request->all();
      $insert = $this->lend->create($dataForm);

      if ($insert)
          return redirect()->route('emprestimo.index')->withSuccess('Empréstimo registrado com sucesso!');
      else {
          return redirect()->back();
      }
  }

  public function show($id)
  {
      //
  }

  public function edit($id)
  {
      $lends = Lend::find($id);
      $members  =  Member::pluck('nome','id')->sortBy('nome');
      $books    =  Book::all()->sortBy('titulo_formatted');
      $title = "Editar Empréstimo";

      return view('lend.cadLend', compact('title', 'lends','members','books'));
  }

  public function update(Request $request, $id)
  {
      $dataForm = $request->all();
      $lends = Lend::find($id);
      $update = $lends->update($dataForm);

      if ($update)
          return redirect()->route('emprestimo.index', $id);
      else
          return redirect()->route('emprestimo.edit', $id)->with(['errors' => 'Falha ao editar']);
  }

  public function destroy($id)
  {
      $lends = Lend::find($id);

      $delete = $lends->delete();

      if ($delete)
          return redirect()->route('emprestimo.index');
      else
          return redirect()->route('emprestimo.index')->with(['errors' => 'Falha ao editar']);
  }
}
