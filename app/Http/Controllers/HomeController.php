<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Book;
use App\Model\Author;
use App\Model\Member;
use App\Model\Lend;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::all();
        $authors = Author::all();
        $members = Member::all();
        $lends    = Lend::all()->sortBy('dataemprestimo')->where('datadevolvido','=','');

        return view('home',compact('books','authors','members','lends'));
    }
}
