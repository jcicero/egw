<?php

namespace App\Http\Controllers;

use App\Model\Author;
use App\Model\Book;
use App\Model\Publisher;
use Illuminate\Http\Request;

class BookController extends Controller
{
    private $book;

    public function __construct(Book $book, Publisher $publisher, Author $author)
    {
        $this->book = $book;
        $this->publisher = $publisher;
        $this->auhtor = $author;
    }
    public function index()
    {
        $books =  Book::sortable()->paginate(50);
        $title = 'Cadastro de Livros';

        return view('book.consBook', compact('title', 'books'));
    }

    public function create()
    {
        $title = 'Cadastro de Livros';
        $publishers = Publisher::all()->sortBy('editora');
        $authors = Author::all()->sortBy('nm_autor');

        return view('book.cadBook', compact('title','publishers','authors'));
    }

    public function store(Request $request)
    {
        $dataForm = $request->all();
        $insert = $this->book->create($dataForm);

        if ($insert)
            return redirect()->route('book.index');
        else {
            return redirect()->back();
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
