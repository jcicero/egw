<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuthorFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
     {
         return [
             'nm_autor' =>'required|min:3|unique:authors',
             'status_id'=>'required',
         ];
     }
     public function messages()
     {
         return [
             'nm_autor.required' => 'O campo AUTOR é de preenchimento obrigatório!',
             'nm_autor.min' => 'O campo AUTOR deve ter pelo menos 3 caracteres!',
             'nm_autor.unique' => 'AUTOR já cadastrado!',
             'status_id.required' => 'Informe o status!',
         ];
     }
}
